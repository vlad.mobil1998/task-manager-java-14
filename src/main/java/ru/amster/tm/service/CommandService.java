package ru.amster.tm.service;

import ru.amster.tm.api.repository.ICommandRepository;
import ru.amster.tm.api.servise.ICommandService;
import ru.amster.tm.dto.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    public String[] getArguments() {
        return commandRepository.getArguments();
    }

}