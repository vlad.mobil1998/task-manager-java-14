package ru.amster.tm.api.servise;

import ru.amster.tm.dto.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArguments();

}