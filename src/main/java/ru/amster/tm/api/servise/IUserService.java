package ru.amster.tm.api.servise;

import ru.amster.tm.entity.User;
import ru.amster.tm.role.Role;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password, Role role);

    User create(String login, String password, String email);

    User create(String login, String password, String email, Role role);

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

}