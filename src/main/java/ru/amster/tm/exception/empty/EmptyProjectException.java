package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public class EmptyProjectException extends AbstractException {

    public EmptyProjectException() {
        super("Error! Project is empty (not found)...");
    }

}