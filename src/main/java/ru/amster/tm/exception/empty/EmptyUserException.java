package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public class EmptyUserException extends AbstractException {

    public EmptyUserException() {
        super("ERROR! User is empty...");
    }

}