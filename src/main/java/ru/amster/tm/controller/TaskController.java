package ru.amster.tm.controller;

import ru.amster.tm.api.controller.ITaskController;
import ru.amster.tm.api.servise.IAuthService;
import ru.amster.tm.api.servise.ITaskService;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    private final IAuthService authService;

    public TaskController(
            ITaskService ITaskService,
            IAuthService authService
    ) {
        this.taskService = ITaskService;
        this.authService = authService;
    }

    private void showTask(final Task task) {
        if (task == null) throw new EmptyTaskException();
        System.out.println("ID: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

    @Override
    public void showTasks() {
        System.out.println("[LIST TASKS]");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final List<Task> tasks = taskService.findAll(userId);
        for (Task task : tasks) System.out.println(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByName() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskService.findOneByName(userId, name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Integer maxIndex = taskService.numberOfAllTasks(userId);
        if (index >= maxIndex) throw new InvalidIndexException(index, maxIndex);
        final Task task = taskService.findOneByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID");
        final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Task task = taskService.findOneById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void clearTask() {
        System.out.println("[CLEAR TASKS]");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        taskService.clear(userId);
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASKS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        taskService.create(userId, name, description);
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskByName() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskService.removeOneByName(userId, name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Integer maxIndex = taskService.numberOfAllTasks(userId);
        if (index >= maxIndex) throw new InvalidIndexException(index, maxIndex);
        final Task task = taskService.removeOneByIndex(userId, index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID");
        final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Task task = taskService.removeOneById(userId, id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER INDEX");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Integer maxIndex = taskService.numberOfAllTasks(userId);
        if (index >= maxIndex) throw new InvalidIndexException(index, maxIndex);
        final Task task = taskService.findOneByIndex(userId, index);
        if (task == null) throw new EmptyTaskException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdate = taskService.updateTaskByIndex(userId, index, name, description);
        if (taskUpdate == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = taskService.findOneById(userId, id);
        if (task == null) return;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdate = taskService.updateTaskById(userId, id, name, description);
        if (taskUpdate == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}