package ru.amster.tm.controller;

import ru.amster.tm.api.controller.IAuthController;
import ru.amster.tm.api.servise.IAuthService;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.exception.empty.EmptyPasswordException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public class AuthController implements IAuthController {

    private final IAuthService authService;

    private boolean loginCheck = false;

    public AuthController(final IAuthService authService) {
        this.authService = authService;
    }

    @Override
    public void login() {
        System.out.println("[LOGIN]");
        if (loginCheck) throw new AccessDeniedException();
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        System.out.println("[ENTER PASSWORD]");
        final String password = TerminalUtil.nextLine();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        authService.login(login, password);
        loginCheck = true;
        System.out.println("[OK]");
    }

    @Override
    public void logout() {
        System.out.println("[LOGOUT]");
        if (!loginCheck) throw new AccessDeniedException();
        authService.logout();
        loginCheck = false;
        System.out.println("[OK]");
    }

    @Override
    public void registration() {
        System.out.println("[REGISTRATION]");
        if (loginCheck) throw new AccessDeniedException();
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        System.out.println("[ENTER PASSWORD]");
        final String password = TerminalUtil.nextLine();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        System.out.println("[ENTER EMAIL]");
        final String email = TerminalUtil.nextLine();
        authService.registration(login, password, email);
        System.out.println("[OK]");
    }

}