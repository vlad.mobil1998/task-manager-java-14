package ru.amster.tm.controller;

import ru.amster.tm.api.servise.IAuthService;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements ru.amster.tm.api.controller.IProjectController {

    private final IProjectService projectService;

    private final IAuthService authService;

    public ProjectController(
            final IProjectService projectService,
            final IAuthService authService
    ) {
        this.projectService = projectService;
        this.authService = authService;
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECT]");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final List<Project> projects = projectService.findAll(userId);
        for (Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void clearProject() {
        System.out.println("[CLEAR PROJECTS]");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        projectService.clear(userId);
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECTS]");
        System.out.println("ENTER NAME:");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.create(userId, name, description);
        System.out.println("[OK]");
    }

    private void showProject(final Project project) {
        if (project == null) throw new EmptyProjectException();
        System.out.println("ID: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectService.findOneById(userId, id);
        if (project == null) {
            System.out.println("[FAIL]");
            throw new EmptyProjectException();
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByName() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectService.findOneByName(userId, name);
        if (project == null) {
            System.out.println("[FAIL]");
            throw new EmptyProjectException();
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Integer maxIndex = projectService.numberOfAllProjects(userId);
        if (index >= maxIndex) throw new InvalidIndexException(index, maxIndex);
        final Project project = projectService.findOneByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
            throw new EmptyProjectException();
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Integer maxIndex = projectService.numberOfAllProjects(userId);
        if (index >= maxIndex) throw new InvalidIndexException(index, maxIndex);
        final Project project = projectService.findOneByIndex(userId, index);
        if (project == null) return;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = projectService.updateProjectByIndex(userId, index, name, description);
        if (projectUpdate == null) {
            System.out.println("[FAIL]");
            throw new EmptyProjectException();
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER ID");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectService.findOneById(userId, id);
        if (project == null) throw new EmptyProjectException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdate = projectService.updateProjectById(userId, id, name, description);
        if (projectUpdate == null) {
            System.out.println("[FAIL]");
            throw new EmptyProjectException();
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Project project = projectService.removeOneByName(userId, name);
        if (project == null) {
            System.out.println("[FAIL]");
            throw new EmptyProjectException();
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Integer maxIndex = projectService.numberOfAllProjects(userId);
        if (index >= maxIndex) throw new InvalidIndexException(index, maxIndex);
        final Project project = projectService.removeOneByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
            throw new EmptyProjectException();
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID");
        final String userId = authService.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectService.removeOneById(userId, id);
        if (project == null) {
            System.out.println("[FAIL]");
            throw new EmptyProjectException();
        }
        System.out.println("[OK]");
    }

}